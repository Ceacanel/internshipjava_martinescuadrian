
public class Rectangle {
	private double line_l,line_L;
	
	public Rectangle(double l,double L)
	{
		// Constructor with arguments
		
		line_l = l;
		line_L = L;
	}
	
	private double Perimeter()
	{
		// Method that return the perimeter of rectangle
		double per;
		per = 2 * (line_l+line_L);
		return per;
	}
	
	private double Area()
	{
		// Method that return the area of rectangle
		double ar;
		ar = line_l * line_L;
		return ar;
	}
	
	public void Display()
	{
		// Display method
		System.out.println("Length: "+ line_L);
		System.out.println("Width: " +line_l);
		System.out.println("Perimetre: "+Perimeter());
		System.out.println("Area: "+ Area());
		
	}
}
