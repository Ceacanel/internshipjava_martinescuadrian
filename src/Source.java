import java.util.Scanner;
public class Source {
	public static void main(String[] argz)
	{
		Scanner input = new Scanner(System.in);
		int check1 = 0;
		while(check1 == 0)
		{
			int check2;
		System.out.println("Enter the number to choose the geometric figures:");
		System.out.println("1 -> Triangle");
		System.out.println("2 -> Circle");
		System.out.println("3 -> Rectangle");
		check2 = input.nextInt();
		if(check2 == 1)
		{
			double T_s1,T_s2,T_s3;
			System.out.print("Enter first side: ");
			T_s1 = input.nextDouble();
			
			System.out.print("Enter second side: ");
			T_s2 = input.nextDouble();
			
			System.out.print("Enter third side: ");
			T_s3 = input.nextDouble();
			// Check if sides are negative or 0
			int checkT1 = 0;
			while(checkT1 == 0)
			{
				if(T_s1 <= 0  || T_s1 == 0)
				{
					System.out.println("Error:First side negative or 0!");
					checkT1 = 0;
					System.out.print("Enter first side: ");
					T_s1 = input.nextDouble();
				}else{
					checkT1 = 1;
				}		
			}
			int checkT2 = 0;
			while(checkT2 == 0)
			{
				if(T_s2 <= 0  || T_s2 == 0)
				{
					System.out.println("Error:Second side negative or 0!");
					checkT2 = 0;
					System.out.print("Enter second side: ");
					T_s2 = input.nextDouble();
				}else{
					checkT2 = 1;
				}		
			}
			int checkT3 = 0;
			while(checkT3 == 0)
			{
				if(T_s3 <= 0  || T_s3 == 0)
				{
					System.out.println("Error:Third side negative or 0!");
					checkT3 = 0;
					System.out.print("Enter Third side: ");
					T_s3 = input.nextDouble();
				}else{
					checkT3 = 1;
				}
			}
			Triangle Obj_T = new Triangle(T_s1,T_s2,T_s3);
			Obj_T.Display();
			check1 = 1;
		}else{
			if(check2 == 2)
			{
				double r;
				System.out.print("Enter radius: ");
				r = input.nextDouble();
				// Check if radius is negative or 0
				int checkC = 0;
				while(checkC == 0)
				{
					if(r == 0)
					{
						System.out.println("Error: Radius cannot be 0");
						checkC = 0;
						System.out.print("Enter radius: ");
						r = input.nextDouble();
					}else{
						if(r <= 0){
							System.out.println("Error: Negative radius");
							checkC = 0;
							System.out.print("Enter radius: ");
							r = input.nextDouble();
						}else{
							checkC = 1;
						}
					}		
				}
				
				Circle Obj_C = new Circle(r);
				Obj_C.Display();
				check1 = 1;
			}else{
				if(check2 == 3)
				{
					double L,l;
					System.out.print("Enter the length: ");
					L = input.nextDouble();
					System.out.print("Enter the width: ");
					l = input.nextDouble();
					// Check if length or width are negative or 0
					int checkR1 = 0;
					while(checkR1 == 0)
					{
						if(L <= 0 || L == 0)
						{
							System.out.println("Error: Negative lenght or 0");
							checkR1 = 0;
							System.out.print("Enter the length: ");
							L = input.nextDouble();
						}else{
							checkR1 = 1;
						}
					}
					double checkR2 = 0;
					while(checkR2 == 0)
					{
						if(l <= 0 || l == 0)
						{
							System.out.println("Error: Negative width or 0");
							checkR2 = 0;
							System.out.print("Enter the width: ");
							l = input.nextDouble();
						}else{
							checkR2 = 1;
						}
					}
					Rectangle Obj_R = new Rectangle(l,L);
					Obj_R.Display();
					check1 = 1;
				}else{
					check1 = 0;
				}				
			}						
		}			
	}
		input.close();
	}
}
