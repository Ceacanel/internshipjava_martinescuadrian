
public class Triangle {
	
	private double side1,side2,side3;
	
	public Triangle(double s1, double s2,double s3)
	{
		// Constructor with arguments
		
		side1 = s1;
		side2 = s2;
		side3 = s3;
	}
	
	private double Perimeter()
	{
		// Method that return the perimeter of triangle
		double Per;
		Per = side1 + side2 + side3;
		return Per;
	}
	
	private double Area()
	{
		// Method that return area with Heron's formula
		double Ar;
		double p;
		p = (side1+side2+side3)/2;
		Ar = Math.sqrt(p*(p-side1)*(p-side2)*(p-side3));
		return Ar;
	}
	
	public void Display()
	{
		// Display method
		System.out.println("Triangle first side: " + side1);
		System.out.println("Triangle second side: " + side2);
		System.out.println("Triangle third side: " + side3);
		this.Area();
		System.out.println("Triangle area: " + Area());
		this.Perimeter();
		System.out.println("Triangle perimeter: " + Perimeter());
		
	}

}
