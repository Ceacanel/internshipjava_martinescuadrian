public class Circle {
	private static double pi = 3.14;
	private double radius;
	
	
	public Circle(double r)
	{
		// Constructor with arguments
		
		radius = r;
	}
	
	private double Perimeter()
	{
		// Method that return the perimeter of circle
		double per;		
		
		per = 2*pi*radius;
		
		return per;
	}
	
	private double Area()
	{
		// Method that return area of circle
		
		double ar;
		ar = pi*radius*radius;
		return ar;
	}
	
	public void Display()
	{
		// Display method
		
		System.out.println("Radius: "+radius);
		System.out.println("Perimeter of circle: "+Perimeter());
		System.out.println("Area of circle: "+Area());
		
	}
}
